package ru.tinkoff.studentsapp.screens

import io.github.kakaocup.kakao.check.KCheckBox
import io.github.kakaocup.kakao.common.views.KView
import io.github.kakaocup.kakao.recycler.KRecyclerView
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KSnackbar
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.favouritepersons.R
import ru.tinkoff.studentsapp.items.PersonItem
import androidx.test.espresso.action.ViewActions

class PersonListScreen : BaseScreen() {
    private val addPersonButton = KButton { withId(R.id.fab_add_person) }
    private val addPersonByNetworkButton = KButton { withId(R.id.fab_add_person_by_network) }
    private val addPersonManuallyButton = KButton { withId(R.id.fab_add_person_manually) }
    private val sortButton = KButton { withId(R.id.action_item_sort) }
    private val sortBottomSheet = KView { withId(R.id.bottom_sheet) }
    val noPersonText = KTextView { withId(R.id.tw_no_persons) }
    val defaultRadioButton = KCheckBox {withId(R.id.bsd_rb_default)}
    val optionAgeRadioButton = KCheckBox {withId(R.id.bsd_rb_age)}
    val snackbar = KSnackbar()
    val snackbarText = snackbar.text
    val personList = KRecyclerView(
        builder = { withId(R.id.rv_person_list) },
        itemTypeBuilder = { itemType(::PersonItem) }
    )

    fun clickOnAddPersonButton() {
        addPersonButton.click()
    }

    fun clickOnAddPersonByNetworkButton() {
        addPersonByNetworkButton.click()
    }

    fun clickOnAddPersonManuallyButton() {
        addPersonManuallyButton.click()
    }

    fun clickOnSortButton() {
        sortButton.click()
    }

    fun clickOnPersonCardAtPosition(position: Int) {
        personList.childAt<PersonItem>(position) {
            view.perform(ViewActions.click())
        }
    }

    fun sortBottomSheetIsClosed() {
        sortBottomSheet.doesNotExist()
    }

    fun textDisplayed(text: KTextView, value: String) {
        text.isVisible()
        text.hasText(value)
    }

    fun textNotDisplayed(text: KTextView) {
        text.isInvisible()
    }

    fun checkListDisplay(list: KRecyclerView) {
        list.isVisible()
    }

    fun checkListSize(list: KRecyclerView, size: Int) {
        list.hasSize(size)
    }

    fun checkOptionSelection(radioButtonOption: KCheckBox) {
        radioButtonOption.perform { isChecked() }
    }

    fun clickOnOption(option: KCheckBox) {
        option.click()
    }

    fun checkSnackbarText(textOnSnackbar: KTextView, expectedText: String) {
        textOnSnackbar.hasText(expectedText)
    }

    fun snackbarDisplayed(snackbar: KSnackbar) {
        snackbar.isDisplayed()
    }

    companion object {
        inline operator fun invoke(crossinline block: PersonListScreen.() -> Unit) =
            PersonListScreen().block()
    }
}
