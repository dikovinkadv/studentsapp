package ru.tinkoff.studentsapp.screens

import io.github.kakaocup.kakao.edit.KEditText
import io.github.kakaocup.kakao.text.KButton
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.favouritepersons.R

class EditPersonInfoScreen : BaseScreen() {
    private val submitButton = KButton { withId(R.id.submit_button) }
    val personNameField  = KEditText { withId(R.id.et_name) }
    val personSurnameField = KEditText { withId(R.id.et_surname) }
    val personGenderField = KEditText { withId(R.id.et_gender) }
    val personDateOfBirthField = KEditText { withId(R.id.et_birthdate) }
    val personEmailField = KEditText { withId(R.id.et_email) }
    val personAddressField = KEditText { withId(R.id.et_address) }
    val personPhoneField = KEditText { withId(R.id.et_phone) }
    val personScoreField = KEditText { withId(R.id.et_score) }
    val screenTitle = KTextView { withId(R.id.tw_person_screen_title) }
    val personPhotoUrlField = KEditText { withId(R.id.et_image) }
    val genderErrorMessage = KTextView { withText(GENDER_ERROR_MESSAGE) }

    fun clickSaveButton() {
        submitButton.click()
    }

    fun clickOnField(field: KEditText) {
        field.click()
    }

    fun clearField(field: KEditText) {
        field.clearText()
    }

    fun checkTextInField(field: KEditText, value: String) {
        field {
            isVisible()
            hasText(value)
        }
    }

    fun enterTextInField(field: KEditText, value: String) {
        field.replaceText(value)
    }

    fun textDisplayed(text: KTextView) {
        text.isDisplayed()
    }

    fun errorNotDisplayed(errorMessage: KTextView) {
        errorMessage.doesNotExist()
    }

    fun replaceTextInField(field: KEditText, text: String) {
        field.replaceText(text)
        Thread.sleep(5000)
    }

    companion object {
        inline operator fun invoke(crossinline block: EditPersonInfoScreen.() -> Unit) =
            EditPersonInfoScreen().block()
        const val GENDER_ERROR_MESSAGE = "Поле должно быть заполнено буквами М или Ж"
    }
}
