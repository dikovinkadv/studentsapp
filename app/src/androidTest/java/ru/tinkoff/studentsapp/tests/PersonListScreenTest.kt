package ru.tinkoff.studentsapp.tests

import androidx.test.espresso.action.ViewActions
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule
import org.junit.Test
import org.junit.After
import org.junit.Before
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.R
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_ADDRESS
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_DATE_OF_BIRTH
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_EMAIL
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_GENDER
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_LAST_NAME
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_PHONE_NUMBER
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_PHOTO_URL
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_SCORE
import ru.tinkoff.studentsapp.data.PersonTestData.TEST_NAME
import ru.tinkoff.studentsapp.rules.LocalhostPreferenceRule
import ru.tinkoff.studentsapp.items.PersonItem
import ru.tinkoff.studentsapp.scenario.ClickOnAddPersonByNetworkButtonScenario
import ru.tinkoff.studentsapp.scenario.CreatePersonScenario
import ru.tinkoff.studentsapp.screens.EditPersonInfoScreen
import ru.tinkoff.studentsapp.screens.PersonListScreen
import ru.tinkoff.studentsapp.utils.countAgeFromDateOfBirth
import ru.tinkoff.studentsapp.utils.mockGetPersonApiResponse
import ru.tinkoff.studentsapp.utils.mockGetThreePersonsApiResponse
import ru.tinkoff.studentsapp.utils.mockResponseCode
import java.net.HttpURLConnection

class PersonListScreenTest : TestCase() {
    @get: Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    private lateinit var db: PersonDataBase

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    //Работает
    //Кейс 1. Проверка скрытия сообщения об отсутствии студентов
    @Test
    fun hidingMessageAboutAbsenceStudents() =
        run {
            mockGetPersonApiResponse()
            PersonListScreen {
                step("Проверка что отображается сообщение об отсутствии студентов") {
                    textDisplayed(noPersonText, NO_PERSON_TEXT)
                }
                step("Нажать на кнопку добавления нового студента") {
                    clickOnAddPersonButton()
                }
                step("Нажать на кнопку загрузки студента из интернета (облачко)") {
                    clickOnAddPersonByNetworkButton()
                }
                step("ОР: Сообщение об отсутствии студентов более не отображается") {
                    textNotDisplayed(noPersonText)
                }
            }
        }

    //Работает
    //Кейс 2. Проверка удаления студента
    @Test
    fun deletingStudent() =
        run {
            mockGetPersonApiResponse()
            PersonListScreen {
                scenario(
                    ClickOnAddPersonByNetworkButtonScenario(repeatCount = REPEAT_TIMES_VALUE_3))
                step("Проверка, что список студентов отображается") {
                    checkListDisplay(personList)
                    checkListSize(personList, PERSON_LIST_SIZE)
                }
                step("Свайпнуть по первому студенту влево, чтобы удалить его") {
                    personList.childAt<PersonItem>(PERSON_ITEM_POSITION_0) {
                        view.perform(ViewActions.swipeLeft())
                    }
                }
                step("ОР: Студент более не отображается, список стал короче") {
                    checkListSize(personList, PERSON_LIST_SIZE_AFTER_DELETING)
                }
            }
        }

    //Работает
    //Кейс 3. Проверка выбора по умолчанию в окне сортировки
    @Test
    fun checkDefaultSelectionInSortWindow() =
        run {
            PersonListScreen {
                step("Нажать на иконку сортировки") {
                    clickOnSortButton()
                }
                step("ОР: Выбран вариант сортировки 'По умолчанию'") {
                    checkOptionSelection(defaultRadioButton)
                }
            }
        }

    //Работает
    //Кейс 4. Проверка сортировки по возрасту
    @Test
    fun checkSortingByAge() =
        run {
            mockGetThreePersonsApiResponse()
            PersonListScreen {
                step("Добавить 3 пользователя") {
                    scenario(
                        ClickOnAddPersonByNetworkButtonScenario(repeatCount = REPEAT_TIMES_VALUE_3))
                    }
                step("Нажать на иконку сортировки") {
                    clickOnSortButton()
                    Thread.sleep(1000)
                }
                step("Выбрать вариант 'По возрасту'") {
                    flakySafely(timeoutMs = 5000) {
                        clickOnOption(optionAgeRadioButton)
                    }
                }
                step("ОР: Экран сортировки закрылся, элементы по убыванию возраста") {
                    flakySafely(timeoutMs = 5000) {
                        sortBottomSheetIsClosed()
                    }
                    checkAgeAtPosition(PERSON_ITEM_POSITION_0, BERTRAM_AGE)
                    checkAgeAtPosition(PERSON_ITEM_POSITION_1, FLENN_AGE)
                    checkAgeAtPosition(PERSON_ITEM_POSITION_2, CAMILLE_AGE)
                }
            }
        }

    //Кейс 6. Проверка редактирования студента
    @Test
    fun editStudentTest() {
        mockGetPersonApiResponse()
        run {
            PersonListScreen {
                scenario(
                    ClickOnAddPersonByNetworkButtonScenario(repeatCount = REPEAT_TIMES_VALUE_1)
                )
                step("Нажать на одного из пользователей.") {
                    clickOnPersonCardAtPosition(PERSON_ITEM_POSITION_0)
                }
            }
            EditPersonInfoScreen {
                step("Проверить, что открылся экран редактирования") {
                    textDisplayed(screenTitle)
                }
                step("Ввести “Иосиф” в поле 'Имя'") {
                    replaceTextInField(personNameField, TEST_NAME)
                }
                step("Нажать кнопку \"Сохранить\".") {
                    clickSaveButton()
                }
            }
            PersonListScreen {
                step(
                    "Открывается главный экран, изменилось имя на 'Иосиф'") {
                    personList.childAt<PersonItem>(PERSON_ITEM_POSITION_0) {
                            personName.hasText(FULL_TEST_NAME)
                        }
                    }
                }
            }
        }

    //Работает
    //Кейс 7. Проверка добавления студента
    @Test
    fun createPersonManualTest() {
        run {
            step("На открывшемся экране заполнить поля валидными данными") {
                scenario(
                    CreatePersonScenario(
                        name = TEST_NAME,
                        lastName = PERSON_LAST_NAME,
                        email = PERSON_EMAIL,
                        address = PERSON_ADDRESS,
                        phone = PERSON_PHONE_NUMBER,
                        score = PERSON_SCORE,
                        photo = PERSON_PHOTO_URL,
                        dateOfBirth = PERSON_DATE_OF_BIRTH,
                        gender = PERSON_GENDER
                    )
                )
            }
            PersonListScreen {
                step(
                    "Появился добавленный пользователь с корректно введенными данными и " +
                            "правильно подсчитанным возрастом"
                ) {
                    personList.childAt<PersonItem>(PERSON_ITEM_POSITION_0) {
                        checkTextPersonItem(personName, FULL_TEST_NAME)
                        checkTextPersonItem(personPrivateInfo, personPrivateInfoText)
                    }
                }
            }
        }
    }

    //Работает
    //Кейс 10. Проверка отображения сообщения об ошибке интернет-соединения
    @Test
    fun internetErrorTextMessageTest() {
        mockResponseCode(HttpURLConnection.HTTP_NO_CONTENT)
        run {
            PersonListScreen {
                scenario(ClickOnAddPersonByNetworkButtonScenario(repeatCount = REPEAT_TIMES_VALUE_1))
                step("ОР: Появляется уведомление 'Internet error! Check your connection.'") {
                    snackbarDisplayed(snackbar)
                    checkSnackbarText(snackbarText, SNACKBAR_ERROR_TEXT)
                }
            }
        }
    }

    private companion object {
        val NO_PERSON_TEXT = InstrumentationRegistry.getInstrumentation()
            .targetContext.getString(R.string.start_screen_no_persons_text)
        const val PERSON_LIST_SIZE = 3
        const val PERSON_LIST_SIZE_AFTER_DELETING = 2
        const val PERSON_ITEM_POSITION_0 = 0
        const val PERSON_ITEM_POSITION_1 = 1
        const val PERSON_ITEM_POSITION_2 = 2
        const val REPEAT_TIMES_VALUE_3 = 3
        const val REPEAT_TIMES_VALUE_1 = 1
        const val SNACKBAR_ERROR_TEXT = "Internet error! Check your connection"
        const val FULL_TEST_NAME = "Иосиф Graves"
        val personAge = countAgeFromDateOfBirth(PERSON_DATE_OF_BIRTH)
        val personPrivateInfoText = "Male, $personAge"
        const val FLENN_AGE = "69"
        const val BERTRAM_AGE = "74"
        const val CAMILLE_AGE = "58"
    }

    private fun checkAgeAtPosition(position: Int, age: String) {
        PersonListScreen {
            personList.childAt<PersonItem>(position) {
                personPrivateInfo.containsText(age)
            }
        }
    }
}
