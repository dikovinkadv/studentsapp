package ru.tinkoff.studentsapp.tests

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule
import org.junit.Test
import org.junit.After
import org.junit.Before
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.rules.RuleChain
import ru.tinkoff.favouritepersons.presentation.activities.MainActivity
import ru.tinkoff.favouritepersons.room.PersonDataBase
import ru.tinkoff.studentsapp.data.PersonTestData.INVALID_PERSON_GENDER
import ru.tinkoff.studentsapp.data.PersonTestData.TEST_NAME
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_ADDRESS
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_DATE_OF_BIRTH
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_EMAIL
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_FIRST_NAME
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_GENDER
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_LAST_NAME
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_PHONE_NUMBER
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_PHOTO_URL
import ru.tinkoff.studentsapp.data.PersonTestData.PERSON_SCORE
import ru.tinkoff.studentsapp.rules.LocalhostPreferenceRule
import ru.tinkoff.studentsapp.scenario.CreatePersonScenario
import ru.tinkoff.studentsapp.screens.EditPersonInfoScreen
import ru.tinkoff.studentsapp.screens.EditPersonInfoScreen.Companion.GENDER_ERROR_MESSAGE
import ru.tinkoff.studentsapp.screens.PersonListScreen
import ru.tinkoff.studentsapp.utils.mockGetPersonApiResponse

class EditInfoPageTest : TestCase() {
    @get: Rule
    val ruleChain: RuleChain = RuleChain.outerRule(LocalhostPreferenceRule())
        .around(WireMockRule(5000))
        .around(ActivityScenarioRule(MainActivity::class.java))

    private lateinit var db: PersonDataBase

    @Before
    fun createDb() {
        db = PersonDataBase.getDBClient(InstrumentationRegistry.getInstrumentation().targetContext)
    }

    @After
    fun clearDB() {
        db.personsDao().clearTable()
    }

    //Работает
    //Кейс 5. Проверка открытия второго экрана с данными пользователя
    @Test
    fun editPersonInfoScreenTest() {
        mockGetPersonApiResponse()
        run {
            PersonListScreen {
                step("Нажать на кнопку добавления нового студента") {
                    clickOnAddPersonButton()
                }
                step("Нажать на кнопку загрузки студента из интернета (облачко)") {
                    clickOnAddPersonByNetworkButton()
                }
                step("Нажать на одного из пользователей.") {
                    clickOnPersonCardAtPosition(0)
                }
            }
            EditPersonInfoScreen {
                step("Открыт экран редактирования, корректно заполнены поля") {
                    checkTextInField(personNameField, PERSON_FIRST_NAME)
                    checkTextInField(personSurnameField, PERSON_LAST_NAME)
                    checkTextInField(personGenderField, PERSON_GENDER)
                    checkTextInField(personDateOfBirthField, PERSON_DATE_OF_BIRTH)
                }
            }
        }
    }

    //Работает
    //Кейс 8. Проверка отображения сообщения об ошибке
    @Test
    fun checkDisplayedGenderErrorMessageTest() {
        run {
            PersonListScreen {
                step("Нажать на кнопку добавления нового студента") {
                    clickOnAddPersonButton()
                }
                step("Нажать на кнопку добавления пользователя вручную (рука)") {
                    clickOnAddPersonManuallyButton()
                }
                EditPersonInfoScreen {
                step("Нажать 'Сохранить'") {
                    clickSaveButton()
                }
                step("Рядом с полем 'Пол' отображается ошибка $GENDER_ERROR_MESSAGE") {
                    textDisplayed(genderErrorMessage)
                }
            }
        }
    }
}

    //Работает
    //Кейс 9. Проверка скрытия сообщения об ошибке при вводе данных в поле
    @Test
    fun genderFieldInputValidationTest() {
        run {
            EditPersonInfoScreen {
                step("Заполнить поля валидными данными. Кроме поля Пол") {
                    scenario(
                        CreatePersonScenario(
                            name = TEST_NAME,
                            lastName = PERSON_LAST_NAME,
                            email = PERSON_EMAIL,
                            address = PERSON_ADDRESS,
                            phone = PERSON_PHONE_NUMBER,
                            score = PERSON_SCORE,
                            photo = PERSON_PHOTO_URL,
                            dateOfBirth = PERSON_DATE_OF_BIRTH,
                            gender = INVALID_PERSON_GENDER
                        )
                    )
                }
                step("Рядом с полем 'Пол' отображается ошибка $GENDER_ERROR_MESSAGE") {
                    textDisplayed(genderErrorMessage)
                }
                step("Нажать на поле Пол") {
                    clickOnField(personGenderField)
                }
                step("Удалить ранее введенные некорректные данные в поле.") {
                    clearField(personGenderField)
                }
                step("Рядом с полем 'Пол' не отображается ошибка $GENDER_ERROR_MESSAGE") {
                    errorNotDisplayed(genderErrorMessage)
                }
            }
        }
    }
}
