package ru.tinkoff.studentsapp.data

object PersonTestData {
    // Валидные данные
    const val TEST_NAME = "Иосиф"
    const val PERSON_FIRST_NAME = "Flenn"
    const val PERSON_LAST_NAME = "Graves"
    const val PERSON_GENDER = "М"
    const val PERSON_DATE_OF_BIRTH = "1954-08-29"
    const val PERSON_ADDRESS = "Hollywood, Los Angeles"
    const val PERSON_PHONE_NUMBER = "322223322"
    const val PERSON_SCORE = "99"
    const val PERSON_EMAIL = "inglorious.basterds@hollywood.com"
    const val PERSON_PHOTO_URL = "https://avatars.mds.yandex.net/get-kinopoisk-image/1773646/c5eef897-dfb2-42a3-bc17-d5346f5dc587/280x420"
    // Невалидные данные
    const val INVALID_PERSON_GENDER = "K"
}
