package ru.tinkoff.studentsapp.items

import android.view.View
import io.github.kakaocup.kakao.recycler.KRecyclerItem
import io.github.kakaocup.kakao.text.KTextView
import ru.tinkoff.favouritepersons.R
import org.hamcrest.Matcher

class PersonItem(matcher: Matcher<View>) : KRecyclerItem<PersonItem>(matcher) {
    val personName = KTextView(matcher) { withId(R.id.person_name) }
    val personPrivateInfo = KTextView(matcher) { withId(R.id.person_private_info) }

    fun checkTextPersonItem(item: KTextView, text: String) {
        item.hasText(text)
    }
}
