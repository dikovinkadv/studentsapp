package ru.tinkoff.studentsapp.scenario

import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext
import ru.tinkoff.studentsapp.screens.EditPersonInfoScreen
import ru.tinkoff.studentsapp.screens.PersonListScreen

class CreatePersonScenario(
    private val name: String,
    private val lastName: String,
    private val email: String,
    private val address: String,
    private val phone: String,
    private val score: String,
    private val photo: String,
    private val dateOfBirth: String,
    private val gender: String
    ) : Scenario() {
    override val steps: TestContext<Unit>.() -> Unit = {
        PersonListScreen {
            step("Нажать на кнопку добавления нового студента") {
                clickOnAddPersonButton()
            }
            step("Нажать на кнопку добавления пользователя вручную (рука)") {
                clickOnAddPersonManuallyButton()
            }
        }
        EditPersonInfoScreen {
            step("Заполнить поля данными") {
                enterTextInField(personNameField, name)
                enterTextInField(personSurnameField, lastName)
                enterTextInField(personEmailField, email)
                enterTextInField(personAddressField, address)
                enterTextInField(personPhoneField, phone)
                enterTextInField(personScoreField, score)
                enterTextInField(personPhotoUrlField, photo)
                enterTextInField(personDateOfBirthField, dateOfBirth)
                enterTextInField(personGenderField, gender)
            }
            step("Нажать сохранить") {
                clickSaveButton()
            }
        }
    }
}
