package ru.tinkoff.studentsapp.scenario

import com.kaspersky.kaspresso.testcases.api.scenario.Scenario
import com.kaspersky.kaspresso.testcases.core.testcontext.TestContext
import ru.tinkoff.studentsapp.screens.PersonListScreen

class ClickOnAddPersonByNetworkButtonScenario(
    private val repeatCount: Int
) : Scenario() {
    override val steps: TestContext<Unit>.() -> Unit = {
        PersonListScreen {
            step("Нажать на кнопку добавления нового студента") {
                clickOnAddPersonButton()
            }
            step("Нажать на кнопку загрузки студента из интернета (облачко)") {
                repeat(repeatCount) {
                    clickOnAddPersonByNetworkButton()
                }
            }
        }
    }
}
