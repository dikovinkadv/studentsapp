package ru.tinkoff.studentsapp.utils

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.get
import com.github.tomakehurst.wiremock.client.WireMock.ok
import com.github.tomakehurst.wiremock.client.WireMock.status
import com.github.tomakehurst.wiremock.client.WireMock.stubFor
import com.github.tomakehurst.wiremock.stubbing.Scenario
import java.io.BufferedReader
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.time.LocalDate
import java.time.Period

fun fileToString(
    path: String,
    context: Context = InstrumentationRegistry.getInstrumentation().targetContext): String {
    return BufferedReader(
        InputStreamReader(context.assets.open(path),
            StandardCharsets.UTF_8)).readText()
}

fun mockGetPersonApiResponse() {
    stubFor(
        get(WireMock.urlEqualTo("/api/"))
            .willReturn(
                ok(fileToString("mock/mock-person-1.json"))
            )
    )
}

fun mockGetThreePersonsApiResponse() {
    stubFor(
        get(WireMock.urlEqualTo("/api/"))
            .inScenario("GetPersons")
            .whenScenarioStateIs(Scenario.STARTED)
            .willSetStateTo("Step 1")
            .willReturn(
                ok(fileToString("mock/mock-person-1.json"))
            )
    )
    stubFor(
        get(WireMock.urlEqualTo("/api/"))
            .inScenario("GetPersons")
            .whenScenarioStateIs("Step 1")
            .willSetStateTo("Step 2")
            .willReturn(
                ok(fileToString("mock/mock-person-2.json"))
            )
    )
    stubFor(
        get(WireMock.urlEqualTo("/api/"))
            .inScenario("GetPersons")
            .whenScenarioStateIs("Step 2")
            .willSetStateTo("Step 3")
            .willReturn(
                ok(fileToString("mock/mock-person-3.json"))
            )
    )
}

fun mockResponseCode(responseStatusCode: Int) {
    stubFor(
        get(WireMock.urlEqualTo("/api/"))
            .willReturn(
                status(responseStatusCode)
            )
    )
}

fun countAgeFromDateOfBirth(date: String): Int {
    val dateArray = date.split("-").map { it.toInt() }
    val year = dateArray[0]
    val month = dateArray[1]
    val day = dateArray[2]
    return getAge(year, month, day)
}

fun getAge(year: Int, month: Int, dayOfMonth: Int): Int {
    return Period.between(
        LocalDate.of(year, month, dayOfMonth),
        LocalDate.now()
    ).years
}
